package de.hanno.application.graphics.config;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class Config {
    public int WIDTH = 1280;
    public int HEIGHT = 720;

    @Inject
    public Config() {}
}
