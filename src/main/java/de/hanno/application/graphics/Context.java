package de.hanno.application.graphics;

import de.hanno.application.graphics.config.Config;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import javax.inject.Inject;
import javax.inject.Singleton;

import static org.lwjgl.opengl.Display.create;
import org.lwjgl.opengl.GL11;

@Singleton
public class Context {

    @Inject Config config;

    public Context() {
        new Thread(() -> {
            try {
                Display.setDisplayMode(new DisplayMode(config.WIDTH, config.HEIGHT));
                create();

                GL11.glMatrixMode(GL11.GL_PROJECTION);
                GL11.glLoadIdentity();
                GL11.glOrtho(0, config.WIDTH, config.HEIGHT, 0, 1, -1);
                GL11.glMatrixMode(GL11.GL_MODELVIEW);
                GL11.glColor3f(1,1,1);

                while (!Display.isCloseRequested()) {
                    GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);

                    GL11.glBegin(GL11.GL_LINES);
                    for(int i = 0; i < config.WIDTH; i+=2) {
                        for(int x = 0; x < config.HEIGHT; x+=2) {
                            GL11.glColor3f((float)i/255f,(float)x/255f,(float)1-x/255f);
                            GL11.glVertex2f(config.WIDTH/2, config.HEIGHT/2);
                            GL11.glVertex2f(i, x);
                        }
                    }
                    GL11.glEnd();

                    Display.update();
                }
            } catch (LWJGLException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
