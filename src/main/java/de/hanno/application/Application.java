package de.hanno.application;

import dagger.Module;
import de.hanno.application.graphics.Context;

import javax.inject.Inject;
import java.time.Duration;
import java.time.LocalDateTime;

@Module(injects = Application.class)
public class Application implements Runnable{
    @Inject
    protected Context graphicsContext;

    private Thread thread;

    LocalDateTime currentTime;
    LocalDateTime lastTime;

    public Application() {
        thread = new Thread(this);
        thread.setDaemon(true);
        currentTime = LocalDateTime.now();
        lastTime = LocalDateTime.now();
    }

    public void start() {
        lastTime = LocalDateTime.now();
        currentTime = LocalDateTime.now();
        thread.start();
    }

    public void update(float seconds) {
        currentTime.plusSeconds((long) seconds);
        System.out.println("Updated with " + seconds + " ...");
    }

    @Override
    public void run() {
        while (true) {
            currentTime = LocalDateTime.now();

            Duration duration = Duration.between(lastTime, currentTime);

            update(duration.getSeconds());

            lastTime = LocalDateTime.now();
        }
    }
}
