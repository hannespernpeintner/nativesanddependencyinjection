package de.hanno;

import dagger.ObjectGraph;
import de.hanno.application.Application;

public class Main {

    public static void main(String[] args) {

        ObjectGraph objectGraph = ObjectGraph.create(new Application());
        Application application = objectGraph.get(Application.class);
        application.start();
    }
}
